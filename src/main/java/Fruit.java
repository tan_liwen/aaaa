import lombok.Data;

/**
 * @Description: TODO
 * @Author: TanLiWen
 * @DateTime: 2024/4/29 18:31
 */
@Data
public class Fruit {
    private String name;
    private Double price;

    public Fruit(String name, Double price) {
        this.name = name;
        this.price = price;
    }

    public Double calcPrice(Double price, Double weight) {
        return price * weight;
    }
}
