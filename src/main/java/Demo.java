import lombok.Data;

/**
 * @Description: TODO
 * @Author: TanLiWen
 * @DateTime: 2024/4/29 18:43
 */
public class Demo {

    public static void main(String[] args) {
        Fruit apple = new Fruit("苹果", 10D);
        Fruit strawberry = new Fruit("草莓", 13D);
        calcAppleAndStrawberryPrice(apple, strawberry);
        Fruit mango = new Fruit("芒果", 20D);
        calcFruitTotal(apple, strawberry, mango);
        Fruit strawberryDiscount = new Strawberry("折扣草莓", 13D);
        Double totalPrice = calcFruitTotal(apple, strawberryDiscount, mango);
        if (totalPrice >= 100) {
            totalPrice -= 10;
        }
        System.out.println(totalPrice);
    }

    private static void calcAppleAndStrawberryPrice(Fruit apple, Fruit strawberry) {
        Double appleWeight = 3D;
        Double strawberryWeight = 2D;
        Double applePrice = apple.calcPrice(apple.getPrice(), appleWeight);
        Double strawberryPrice = strawberry.calcPrice(strawberry.getPrice(), strawberryWeight);
        Double totalPrice = applePrice + strawberryPrice;
        System.out.println(totalPrice);
    }

    private static Double calcFruitTotal(Fruit apple, Fruit strawberry, Fruit mango) {
        Double appleWeight = 2D;
        Double strawberryWeight = 2D;
        Double mangoWeight = 3D;
        Double applePrice = apple.calcPrice(apple.getPrice(), appleWeight);
        Double strawberryPrice = strawberry.calcPrice(strawberry.getPrice(), strawberryWeight);
        Double mangoPrice = mango.calcPrice(mango.getPrice(), mangoWeight);
        Double totalPrice = applePrice + strawberryPrice + mangoPrice;
        System.out.println(totalPrice);
        return totalPrice;
    }

    public static class Strawberry extends Fruit {
        public Strawberry(String name, Double price) {
            super(name, price);
        }

        @Override
        public Double calcPrice(Double price, Double weight) {
            return price * weight * 0.8;
        }
    }
}
